
#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@protocol SaveUpdatedDataDelegate <NSObject>

- (void) saveUserName:(NSString*)username;

@end

@interface AccountView : UIView {
    
}

@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UITextField *usernameTF;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@property (weak, nonatomic) id <SaveUpdatedDataDelegate> delegate;

@end

