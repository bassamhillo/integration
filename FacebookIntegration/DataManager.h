//
//  DataManager.h
//  FacebookIntegration
//
//  Created by Julia on 8/26/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataManager : NSObject {
    
}
@property (retain, nonatomic) NSURLConnection *connection;

- (void) sendUpdateRequest:(NSString*) username;
@end

NS_ASSUME_NONNULL_END
