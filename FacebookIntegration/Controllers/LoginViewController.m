//
//  ViewController.m
//  FacebookIntegration
//
//  Created by Julia on 8/24/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import "LoginViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AccountViewController.h"
@import GoogleSignIn;

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpFacebookSignInBtn];
    [self setUpGoogleSignInBtn];

    
}

- (void)viewDidAppear:(BOOL)animated {
    if ([FBSDKAccessToken currentAccessToken]) {
        [self presentAccountView:Facebook];
    }

    [[GIDSignIn sharedInstance] restorePreviousSignIn];
}


- (void)setUpFacebookSignInBtn {
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.delegate = self;
    loginButton.permissions = @[@"public_profile", @"email"];

    loginButton.center = self.facebookLoginBtn.center;
    [self.view addSubview:loginButton];
}

//MARK: - Facebook login button delegate
- (void)loginButton:(nonnull FBSDKLoginButton *)loginButton didCompleteWithResult:(nullable FBSDKLoginManagerLoginResult *)result error:(nullable NSError *)error {
    if (error) {
        NSLog(@"Error : %@",error);
    }
    if (result.isCancelled) {
        NSLog(@"User cancle the login");
    } else if (result.declinedPermissions.count > 0) {
        NSLog(@"User declined the permisstions");
    } else {
        
        [self presentAccountView:Facebook];
    }
}

- (void)loginButtonDidLogOut:(nonnull FBSDKLoginButton *)loginButton {
    
}

- (void)presentAccountView:(LoginFrom)from {
    AccountViewController *accountViewController = [[AccountViewController alloc]init];
    [accountViewController setLoginFrom:from];

    UINavigationController *accountVC = [[UINavigationController alloc] initWithRootViewController:accountViewController];
    [accountVC setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:accountVC animated:YES completion:nil];
}

// MARK: - Google sign in Button
-(void) setUpGoogleSignInBtn {
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].presentingViewController = self;

    GIDSignInButton *googleBtn = [[GIDSignInButton alloc] init];
    googleBtn.center = self.googleLoginBtn.center;
    [self.view addSubview:googleBtn];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
  if (error != nil) {
    if (error.code == kGIDSignInErrorCodeHasNoAuthInKeychain) {
      NSLog(@"The user has not signed in before or they have since signed out.");
    } else {
      NSLog(@"%@", error.localizedDescription);
    }
    return;
  }
    [self presentAccountView:Google];
}

@end
