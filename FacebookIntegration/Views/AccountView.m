//
//  AccountView.m
//  FacebookIntegration
//
//  Created by Julia on 8/24/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountView.h"

@interface AccountView()

@end

@implementation AccountView
- (IBAction)saveUpdatesData:(id)sender {
    if (![self.usernameTF.text isEqual:@""]) {
        [self.delegate saveUserName:self.usernameTF.text];
    }
}

@end
