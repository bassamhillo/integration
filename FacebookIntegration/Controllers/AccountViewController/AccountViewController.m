//
//  AccountViewController.m
//  FacebookIntegration
//
//  Created by Julia on 8/24/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import "AccountViewController.h"
#import "DataManager.h"

// Frameworks
#import <FBSDKCoreKit/FBSDKProfile.h>
@import GoogleSignIn;

@interface AccountViewController () {
    LoginFrom loginFrom;
    DataManager *dataManager;
}

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.accountView.delegate = self;
    
    [self fetchData];
    [self setupLogoutButton];
    [self setupEditButton];
    
    dataManager = [[DataManager alloc] init];
}

- (void) setupLogoutButton {
    UIBarButtonItem *logoutBtn =[[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    self.navigationItem.leftBarButtonItem = logoutBtn;
}

- (void) setupEditButton {
    self.accountView.saveBtn.hidden = true;
    
    UIBarButtonItem *editBtn =[[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(edit)];
    self.navigationItem.rightBarButtonItem = editBtn;
}

- (void) logout {
    FBSDKLoginManager *manager;
    switch (loginFrom) {
        case Facebook:
            manager = [[FBSDKLoginManager alloc] init];
            [manager logOut];
            break;
            
        case Google:
            [[GIDSignIn sharedInstance] signOut];
            break;

        default:
            break;
    }

    [self dismissViewControllerAnimated:false completion:nil];
}

- (void) edit {
    self.accountView.usernameTF.enabled = true;
    self.accountView.saveBtn.hidden = false;
}

- (void)setLoginFrom:(LoginFrom)from {
    loginFrom = from;
}

- (void) fetchData {

    GIDGoogleUser *currentUser = [[GIDSignIn sharedInstance]currentUser];
    switch (loginFrom){
        case Google :
            if (currentUser) {
                NSURL *url = [currentUser.profile imageURLWithDimension:(100)];
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
               
                self.accountView.usernameTF.text = [NSString stringWithFormat:@"%@", currentUser.profile.name];
                self.accountView.userImage.image = image;
            }
            break;
        case Facebook:
            dispatch_async(dispatch_get_main_queue(), ^{
                [FBSDKProfile loadCurrentProfileWithCompletion:^(FBSDKProfile * _Nullable profile, NSError * _Nullable error) {
                    if (profile) {
                        NSURL *url = [profile imageURLForPictureMode:FBSDKProfilePictureModeSquare size:CGSizeMake(100,100)];
                        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                        
                        self.accountView.usernameTF.text = [NSString stringWithFormat:@"%@", profile.name];
                        self.accountView.userImage.image = image;
                        
                    }
                }];
            });
            break;
    }

}

- (void)saveUserName:(NSString *)username {
    [dataManager sendUpdateRequest:username];
}

@end
