//
//  ViewController.h
//  FacebookIntegration
//
//  Created by Julia on 8/24/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@import GoogleSignIn;

@interface LoginViewController : UIViewController<FBSDKLoginButtonDelegate,GIDSignInDelegate>
@property (weak, nonatomic) IBOutlet UIView *facebookLoginBtn;
@property (weak, nonatomic) IBOutlet UIView *googleLoginBtn;

@end

