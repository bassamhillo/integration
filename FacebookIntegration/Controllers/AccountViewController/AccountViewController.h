//
//  AccountViewController.h
//  FacebookIntegration
//
//  Created by Julia on 8/24/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountView.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger,LoginFrom) {
    Facebook,
    Google
};

@interface AccountViewController : UIViewController <SaveUpdatedDataDelegate>
@property (strong, nonatomic) IBOutlet AccountView *accountView;

- (void)setLoginFrom:(LoginFrom)from;
@end

NS_ASSUME_NONNULL_END
